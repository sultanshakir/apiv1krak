<?php

include_once 'phpseclib0.3.5/Crypt/AES.php';

class ApiAccessMaker
{
    public function fnEncrypt($sValue, $sSecretKey)
    {
        $aes = new Crypt_AES();

        $aes->setKey($sSecretKey);

        $plaintext = $sValue;

        $encrypted = base64_encode($aes->encrypt($plaintext));

        return $encrypted;
    }

    public function fnDecrypt($sValue, $sSecretKey)
    {
        $aes = new Crypt_AES();

        $aes->setKey($sSecretKey);

        $decrypted = $aes->decrypt(base64_decode($sValue));

        return $decrypted;
    }
}
