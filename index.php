<?php

// TODO: Catch PHP errors such as: Fatal error: Maximum execution time of 900 seconds exceeded in C:\xampp\htdocs\mpages\api\vendor\guzzle\guzzle\src\Guzzle\Http\Curl\CurlMulti.php on line 267
// TODO: API documentation
// TODO: Test Mellinium app server XML submissions
// TODO: Error messaging 500, 404, 403, more?
// TODO: Redirection 301, 302
// TODO: Good 200.  In what cases?
// TODO: Discuss pagination with Chris for large results -> maybe use temp DB
// TODO: Firewall access -> maybe use .htaccess for Access-Control (ex. Header add Access-Control-Allow-Origin "http://echo.aws.com")
// TODO: What metadata would be good for client accessing the API?
// TODO: Later project - Alan's submission via XML
    ini_set('max_execution_time', 900); // 15 minutes
    require_once 'vendor/autoload.php';
    require_once 'common/config.php';
    require_once 'common/ApiAccessMaker.php';

    use \Slim\Slim;
    use \Guzzle\Http\Client;
    use \Guzzle\Http\Message\Request;

    $app = new Slim(
        array(
            'log.writer' => new \Slim\Extras\Log\DateTimeFileWriter(),
            'debug' => false
        )
    );

    $app->get(
        "/:report(/)",
        'requiresHttps',
        'getAuth',
        function ($report) use ($app) {

            $req = $app->request();

            $paramValue = $req->params('json');

            $domainValues = setDefaultDomainValues($req);

            $resBody = makeRequestToMpageWebSrvc(
              $report,
              $paramValue,
              $domainValues
            );

            $res = $app->response();

            // Check if response is JSON or XML
            $contType = isJSONorXML($resBody);

            $res['Content-Type'] = $contType;

            $res->body($resBody);
        }
    );

    $app->post(
        "/mobjects",
        'requiresHttps',
        'getAuth',
        function () use ($app) {

            $req = $app->request();

            $package = $req->params('package');
            $xmlRequestMessage = $req->params('XmlMessage');

            $domainValues = setDefaultDomainValues($req);

            // $package
            // - Combination of Package and Servlet that appends to URL
            // - Ex. com.cerner.encounter.EncounterServlet

            $resBody = mellObjRequest(
              $xmlRequestMessage,
              $package,
              $domainValues
            );

            $res = $app->response();

            // Check if response is JSON or XML
            $contType = isJSONorXML($resBody);

            $res['Content-Type'] = $contType;
            $res->body($resBody);
        }
    );

    $app->post(
        "/:report(/)",
        'requiresHttps',
        'getAuth',
        function ($report) use ($app) {

            $req = $app->request();

            $paramValue = $req->params('patData');

            $domainValues = setDefaultDomainValues($req);

            $resBody = makeRequestToMpageWebSrvc(
              $report,
              $paramValue,
              $domainValues
            );

            $res = $app->response();

            // Check if response is JSON or XML
            $contType = isJSONorXML($resBody);

            $res['Content-Type'] = $contType;
            $res->body($resBody);
        }
    );
// function that determines which domain the user wants to pull data from

function setDefaultDomainValues($req)
{
  $domain = $req->params('domain');
  if (!isset($domain) || $domain == '' || ($domain !== 'cert' && $domain !== 'prod'))
  {
    $domain = 'build';
  }
  
  $domainValues = getDomainEndpoint($domain);
  return $domainValues;
}

function getDomainEndpoint($domain = '')
{
  switch ($domain)
  {
    case 'build':
      return array(
        'endpoint'=> BUILD_MILLOBJ_ENDPOINT,
        'domainUser'=> BUILD_USER,
        'domainUserPassword' => SUPER_USER_PASS
      );
    break;
    case 'cert':
      return array(
        'endpoint'=> CERT_MILLOBJ_ENDPOINT,
        'domainUser'=> CERT_USER,
        'domainUserPassword' => SUPER_USER_PASS
      );
    break;
    case 'prod':
      return array(
        'endpoint'=> PROD_MILLOBJ_ENDPOINT,
        'domainUser'=> PROD_USER,
        'domainUserPassword' => SUPER_USER_PASS
      );
    break;
    default:
      return false;
    break;
  }
}

function validateParameters ($paramsToValidate)
{
    // If valid JSON then return param containing JSON data
    if (json_decode($paramsToValidate)) {
        return $paramsToValidate;
    } else {
        errorHandler(400, INVALID_JSON);
    }

}

function getActualReportName ($uriReportName)
{
    # Gets actual report name from DB
    try {
        $dbh = new PDO('mysql:host='.HOST.';dbname='.DBNAME, DBUSER, DBPASS);
        try {
            $query = 'Select actualreport from reports where urivar=:uriRepName';
            $sth = $dbh->prepare($query);
            $sth->bindParam(':uriRepName', $uriReportName, PDO::PARAM_STR);
            $sth->execute();
            $result = $sth -> fetchColumn();
            if ($result === false) {
                throw new PDOException();
            }
            return $result;
        } catch (PDOException $e) {
            errorHandler(200, REPORT_NOT_FOUND);
        }
    } catch (PDOException $e) {
        errorHandler(200, DB_CONN_ERROR);
    }
}

function errorHandler($error, $message)
{
    $app = Slim::getInstance();
    $res = $app->response();
    $res['Content-Type'] = 'application/json';
    $app->halt($error, '{"Status":"Error","Message":"'.$message.'"}');
}

function getAuth ()
{
    $app = Slim::getInstance();
    $req = $app->request();
    $auth = $req->headers('Authorization');

    $inst = new ApiAccessMaker();
    $hashedAuth = $inst->fnEncrypt($auth, SALT);

    # Checks API key against hashed value in database
    try {
        $dbh = new PDO('mysql:host='.HOST.';dbname='.DBNAME, DBUSER, DBPASS);
        try {
            $query = 'Select ufunguo from wateja where ufunguo=:keyRev';
            $sth = $dbh->prepare($query);
            $sth->bindParam(':keyRev', $hashedAuth, PDO::PARAM_STR);
            $sth->execute();
            $result = $sth -> fetchColumn();
            if ($result === false) {
                throw new PDOException();
            }
            return $result;
        } catch (PDOException $e) {
            errorHandler(401, INVALID_APIKEY);
        }
    } catch (PDOException $e) {
        errorHandler(500, DB_CONN_ERROR);
    }
}

function mellObjRequest($xmlRequestMessage, $package, $domainValues)
{
    $endpoint = $domainValues['endpoint'];
    $domainUser = $domainValues['domainUser'];
    $domainUserPassword = $domainValues['domainUserPassword'];

    $curl = new Curl;
    $curl->headers['Authorization'] = 'Basic '. base64_encode($domainUser.':'.$domainUserPassword);
    $curl->options['CURLOPT_SSL_VERIFYPEER'] = false;
    $response = $curl->post($endpoint . $package, array('XmlMessage'=>$xmlRequestMessage));

    return $response;
}

function makeRequestToMpageWebSrvc($report, $params = null, $domainValues)
{
    // TODO: Fix PHP execution timeout error .htaccess, php.ini, and/or CURL request timeout

    $domainUser = $domainValues['domainUser'];
    $domainUserPassword = $domainValues['domainUserPassword'];

    // Get actual report name from database
    $curReport = getActualReportName($report);


    // Check to see if 'parameters' URI query string is set and 1st character is alphanumeric

    $paramsNow = validateParameters($params);

    $client = new Client(MPAGE_WEBSERVICE_PATH);

    try {

        $url = $curReport ."/?parameters='MINE','".$paramsNow."',''";

        $request = new Request(
            'GET',
            $url
        );

        $query = $request->getQuery()->useUrlEncoding(false);
        $request->setUrl(MPAGE_WEBSERVICE_PATH.$url);
        $request->setAuth($domainUser, $domainUserPassword);
        $request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, false);
        $request->setClient($client);

        $response = $request->send();
        // Only get body of response without headers
        $decodedResponse = $response->getBody();
        return $decodedResponse;
    } catch (Exception $e) {
        return $e->getMessage();
    }
}


function requiresHttps ()
{
    $app = Slim::getInstance();
    $req = $app->request();
    $port = $req->getPort();
    if ($port === 80) {
        errorHandler(505, API_HTTPS_ONLY);
    } elseif ($port === 443) {
            return true;
    } else {
        errorHandler(505, API_HTTPS_ONLY);
    }
};

function isJSONorXML ($resBodyContent='')
{
    // Using try/catch here because server responds with an HTML message
    // when program is unable to run which is not valid JSON nor XML and
    // throws an Exception when XML is not valid
    try {
        if (json_decode($resBodyContent)) {
            return 'application/json';
        } elseif (simplexml_load_string($resBodyContent)) {
            return 'application/xml';
        } else {
            return false;
        }
    } catch (Exception $e) {
        $app = Slim::getInstance();
        $log = $app->log;
        $app->log->error("Response:\n" . $resBodyContent . "\n" . $e->getMessage());
        errorHandler(200, NO_RESULTS);
    } 
}

$app->run();
