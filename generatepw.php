<?php

require_once 'common/ApiAccessMaker.php';
require_once 'common/config.php';

$inst = new ApiAccessMaker();

function randomPassword()
{
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 128; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

if(!isset($_GET['p']))
{
    $newPass = randomPassword();
} else {
    $newPass = $_GET['p'];
}

$encrypted = $inst->fnEncrypt($newPass, SALT);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generate Password</title>
    <script src="common/js/jqueryzclip.js"></script>
    <script>
        $(document).ready(function () {
            $('a#copy-pass').zclip({
                path:'common/ZeroClipboard.swf',
                copy:$('span#pass').text()
            });

            $('a#copy-dbpass').zclip({
                path:'common/ZeroClipboard.swf',
                copy:$('span#dbpass').text()
            });
        });
    </script>
</head>
<body>
<?php
    echo("<b>New Password: </b><br><span id=pass>".$newPass."</span> ");
    echo('<a href="#" id="copy-pass">Copy</a><br><br>');
    echo("<b>Database Hash: </b><br><span id=dbpass>".$encrypted."</span> ");
    echo('<a href="#" id="copy-dbpass">Copy</a><br><br>');
    echo "<b>Decryption check:</b><br>".$inst->fnDecrypt($encrypted, SALT);
?>
</body>
</html>

